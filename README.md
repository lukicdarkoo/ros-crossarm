# ARM ROS packages on x86

- Install Qemu and Docker
- Register Qemu emulator inside Linux kernel (requires >4.9): `docker run --rm --privileged hypriot/qemu-register`
- Fire Docker image: `./run.sh`
- Build the package: `./build.sh`